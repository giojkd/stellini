<?php

namespace App\Traits;
use Str;

trait Helpers
{
    public function getModelName()
    {

        $reflection = new \ReflectionClass($this);
        $modelName = implode("\\", [$reflection->getNamespaceName(),]);
        return Str::lower($reflection->getShortName());
        return $modelName;
    }

    function makeUrl($locale)
    {
        $modelName = Str::lower($this->getModelName());
        return Route($modelName, ['id' => $this->id, 'name' => Str::slug($this->name), 'locale' => $locale]);
    }

    function cover()
    {
        return url('storage/' . collect($this->images)->first());
    }

}
