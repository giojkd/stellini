<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
        $this->crud->setEntityNameStrings('article', 'articles');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Article Name", // Table column heading
            'type' => 'Text'
        ]);

        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Article Intro", // Table column heading
            'type' => 'Text'
        ]);

        $this->crud->addColumn([
            'name' => 'is_blog',
            'type' => 'boolean',
            'label' => "Is blog",
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ArticleRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'is_blog',
            'type' => 'checkbox',
            'label' => "Is blog",
        ]);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => "Title",
        ]);

        $this->crud->addField([
            'label' => 'Excerpt',
            'name' => 'short_description',
            'type' => 'textarea',
        ]);

        $this->crud->addField([
            'label' => 'Content',
            'name' => 'description',
            'type' => 'wysiwyg',
        ]);

        $this->crud->addField([
            'name' => 'tag',
            'type' => 'text',
            'label' => "Tag",
        ]);

$this->crud->addField(
            [
                'name' => 'images', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/uploads/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
            ]
);


    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
