<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArtpieceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ArtpieceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArtpieceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Artpiece::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/artpiece');
        CRUD::setEntityNameStrings('artpiece', 'artpieces');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
     #   CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */

        $this->crud->addColumns([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [  // Select2
                'label'     => "Category",
                'type'      => 'select',
                'name'      => 'artpiececategory_id', // the db column for the foreign key

                // optional
                'entity'    => 'artpiececategory', // the method that defines the relationship in your Model
                'model'     => "App\Models\Artpiececategory", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                #'default'   => 2, // set the default value of the select2

                // also optional
                /* 'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
                */
            ]

        ]);

    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ArtpieceRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [
                'name' => 'short_description',
                'type' => 'textarea',
                'label' => 'Short description'
            ],
            [
                'name' => 'description',
                'type' => 'wysiwyg',
                'label' => 'Description'
            ],
            [
                'name' => 'images', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/uploads/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
            ],
            [  // Select2
                'label'     => "Category",
                'type'      => 'select2',
                'name'      => 'artpiececategory_id', // the db column for the foreign key

                // optional
                'entity'    => 'artpiececategory', // the method that defines the relationship in your Model
                'model'     => "App\Models\Artpiececategory", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                #'default'   => 2, // set the default value of the select2

                // also optional
               /* 'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
                */
            ]

        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {

        $this->setupCreateOperation();
    }
}
