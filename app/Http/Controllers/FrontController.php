<?php

namespace App\Http\Controllers;

use App\Models\Artpiece;
use App\Models\Artpiececategory;
use App\Models\Client;
use App\Models\Photo;
use Illuminate\Http\Request;
use App\Models\Section;
use App\Models\Product;
use App\Models\Video;
use App\Models\Article;
use Auth;

class FrontController extends Controller
{

    //
    public function defaultData($locale){
        $data = [];
        $data['sections'] = Section::get();
        $data['locale'] = $locale;
        $data['clients'] = Client::get();
        $data['articles'] = Article::get();
        return $data;
    }

    public function home($locale = null){
        $data = $this->defaultData($locale);




        if(is_null($locale)){
            return redirect(Route('home',['locale' => env('DEFAULT_LOCALE') ]));
        }

        return view('front.home',$data);
    }
/*
    public function section($locale,$name,$id){
        $section = Section::findOrFail($id);
        dd($section);
    }
*/

    public function products($locale){
        $data = $this->defaultData($locale);
        $data['products'] = Product::get();
        return view('front.products', $data);
    }

    public function product($locale,$name,$id){
        $data = $this->defaultData($locale);
        $data['product'] = Product::findOrFail($id);
        return view('front.product',$data);
    }

    public function artPiecesCategories($locale){

        $data = $this->defaultData($locale);
        $data['artPiecesCategories'] = Artpiececategory::get();
        return view('front.artpieces', $data);
    }

    public function artPieces($locale,$name,$id){
        $data = $this->defaultData($locale);
        $artPiecesCategory = Artpiececategory::findOrFail($id);
        $data['pieces'] = $artPiecesCategory->artpieces;
        return view('front.artpiececategory', $data);

    }

    public function artPiece($locale, $name, $id){
        $data = $this->defaultData($locale);
        $data['piece'] = Artpiece::findOrFail($id);
        return view('front.artpiece', $data);
    }

    public function modelling($locale){
        $data = $this->defaultData($locale);

        return view('front.modelling', $data);
    }

    public function photos($locale){
        $data = $this->defaultData($locale);

        $data['photos'] = Photo::where('is_private',0)->get();
        return view('front.photos', $data);
    }

    public function privatePhotos($locale)
    {
        if(Auth::check()){
            $data = $this->defaultData($locale);
            $data['photos'] = Photo::where('is_private', 1)->get();
            return view('front.photos', $data);
        }
    }

    public function photo($locale, $name, $id){
        $photo = Photo::findOrFail($id);
        if($photo->is_private && !Auth::check()){
            return redirect('/');
        }
        $data = $this->defaultData($locale);
        $data['photo'] = $photo;
        return view('front.photo', $data);
    }

    public function videos($locale)
    {
        $data = $this->defaultData($locale);
        $data['videos'] = Video::get();
        return view('front.videos', $data);
    }

    public function video($locale, $name, $id)
    {
        $data = $this->defaultData($locale);
        $data['video'] = Video::findOrFail($id);
        return view('front.video', $data);
    }

    public function sendRequest(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        \App\Models\Request::create(collect($request->all())->except('_token')->toArray());
        return redirect(Route('thankyou',['locale' => $request->locale ]));
    }

    public function Thankyou($locale){
        $data = $this->defaultData($locale);

        return view('front.thankyou', $data);
    }


}
