<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DropzoneRequest;

class DropzoneUpload extends Controller
{
    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "public"; //
        $destination_path = "media";
        $file = $request->file('file');

        $extension = $file->extension();

        try {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName() . time()) . '.'. $extension;
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path . '/' . $filename]);
        } catch (\Exception $e) {
            return response($e->getMessage(),500);
            /*
            if (empty($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
            */
        }
    }
}
