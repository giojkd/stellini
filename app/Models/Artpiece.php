<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Artpiececategory;
use App\Traits\Helpers;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

class Artpiece extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Helpers;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'artpieces';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $translatable = ['name', 'short_description', 'description'];
    protected $guarded = ['id'];

    protected $casts = ['images' => 'array'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function artpiececategory(){
        return $this->belongsTo(Artpiececategory::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
