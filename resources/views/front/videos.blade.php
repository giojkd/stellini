@extends('front.main')
@section('content')
    @foreach ($videos->chunk(4) as $chunks)
        <div class="container-fluid">
            <div class="row">
            @foreach ($chunks as $video)
                <div class="col-md-3 col-6">
                    <a href="{{ $video->makeUrl($locale) }}">
                        {{ $video->name }}
                        <video controls="false" width="100%" height="320" controls>
                            <source src="{{ url($video->video) }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <img src="{{ $video->cover() }}" alt="" class="img-fluid">
                    </a>
                </div>
            @endforeach
        </div>
        </div>
    @endforeach

@endsection

@push('after-scripts')
        <style>
            video::-webkit-media-controls {
                display: none;
            }
        </style>
@endpush

