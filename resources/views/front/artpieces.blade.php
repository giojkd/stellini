@extends('front.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @foreach ($artPiecesCategories as $cat)
                <div class="col border-right vh-100 text-center d-flex justify-content-center align-items-center">
                    <div class="d-flex">
                        <a href="{{ url($cat->makeUrl($locale)) }}">{{ $cat->name }}</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
