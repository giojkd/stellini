@extends('front.main')
@section('content')
    @foreach ($photos->chunk(4) as $chunks)
        <div class="row">
            @foreach ($chunks as $photo)
                <div class="col-md-3 col-6">
                    <a href="{{ $photo->makeUrl($locale) }}">
                        <img src="{{ $photo->getPhoto() }}" alt="" class="img-fluid">
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach
@endsection
