@extends('front.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1>Thank you</h1>
                <p>
                    We've received your request, and we'll get back to you as soon as possible.
                </p>
            </div>
        </div>
    </div>
@endsection
