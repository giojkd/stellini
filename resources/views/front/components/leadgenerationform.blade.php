<hr>
<div class="bg-light p-3">
    <b>Mi interessa, desidero essere ricontattato</b>
    <form action="{{ Route('sendrequest') }}" method="POST">
        @csrf
        <input type="hidden" name="locale" value="{{ $locale }}">
        <div class="row">
            <div class="col">
                <input type="text" name="name" placeholder="Il suo nome" class="form-control form-control-sm mt-2 rounded-0">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <input type="text" name="email" placeholder="La sua email" class="form-control form-control-sm mt-2 rounded-0">
            </div>
        </div>
        <div class="row">
               <div class="col">
                <input type="text" name="phone" placeholder="Il suo numero di telefono" class="form-control form-control-sm mt-2 rounded-0">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <button type="submit" class="btn rounded-0 btn-dark btn-sm mt-2 ">Invii la sua richiesta di contatto</button>
            </div>
        </div>
    </form>
</div>
