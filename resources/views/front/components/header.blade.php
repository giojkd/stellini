<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300&display=swap" rel="stylesheet">
    <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f8ad98ccd3f1098"></script>
    <style>
        body{
            font-family: 'Roboto', sans-serif;
            font-weight: 300;
            font-size: 14px;
        }
        a{
            color: #000;
        }
        a:hover{
            color: #000;
        }
        .client-logo{
            padding: 10px;
            max-width: 5%;
            margin: 10px;
            -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
            filter: grayscale(100%);
            transition: 0.3s all;
        }

        .client-logo:hover{
            -webkit-filter: grayscale(0%); /* Safari 6.0 - 9.0 */
            filter: grayscale(0%);
        }

        .client-logo:first-of-type{
            margin-left: 0px;
            padding-left: 0px;

        }

        .article{

            margin: 15px;
            padding-left: 0px;
        }

        .article img{
            margin-bottom: 10px;
        }

        .bg-cover{
              -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
            filter: grayscale(100%);
            transition: 0.3s all;
            background-size: cover!important;
            opacity: 0.5;
        }

        .bg-cover:hover{
              -webkit-filter: grayscale(0%); /* Safari 6.0 - 9.0 */
            filter: grayscale(0%);
            opacity: 1;
        }

        .full-opacity{
            letter-spacing: 2px;
            padding: 5px;
            font-size: 36px;
            color: #fff;
                opacity: 1!important;
        }
    </style>
    <title>Riccardo Stellini</title>
  </head>
  <body>

    @include('front.components.navbar')
