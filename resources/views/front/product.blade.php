@extends('front.main')
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-6 text-center">
                @foreach ($product->images as $image)
                    <img src="{{ url('storage/'. $image ) }}" alt="" class="img-fluid">
                @endforeach
            </div>
            <div class="col-md-6">
                <h1 class="mt-4">{{ $product->name }}</h1>
                <div class="photo-description my-4">
                    {!! $product->description !!}
                </div>
                @include('front.components.leadgenerationform')
            </div>

        </div>
    </div>
@endsection
