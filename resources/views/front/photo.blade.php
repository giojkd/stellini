@extends('front.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="{{ $photo->getPhoto() }}" alt="">
            </div>
            <div class="col-md-6">
                <h1 class="mt-4">{{ $photo->name }}</h1>
                <div class="photo-description my-4">
                    {!! $photo->description !!}
                </div>
                @include('front.components.leadgenerationform')
            </div>
        </div>
    </div>
@endsection
