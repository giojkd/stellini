@extends('front.main')
@section('content')
<div class="container-fluid">
    @foreach ($products->chunk(4) as $chunks)
        <div class="row">
            @foreach ($chunks as $product)
                <div class="col-6 col-md-3">
                    <a href="{{ $product->makeUrl($locale) }}" class="product-thumbnail d-block" >
                        <div class="product-thumbnail-head">
                            {{ $product->name }}
                        </div>
                        <div class="product-thumbnail-body">
                            <img class="img-fluid" src="{{ $product->cover() }}" alt="">
                        </div>
                        <div class="product-thumbnail-footer"></div>
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach
</div>
@endsection
