@extends('front.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @foreach ($sections as $section)
                <div style="background: url('{{ url($section->cover()) }}') no-repeat center center" class="bg-cover col border-right vh-100 text-center d-flex justify-content-center align-items-center">
                    <div class="d-flex">
                        {{-- <a href="{{ Route('section',['locale' => $locale,'name'=>Str::slug($section->name),'id'=>$section->id]) }}">{{ $section->name }}</a>  --}}
                        <a class="full-opacity" href="{{ url($section->url) }}">{{ Str::upper( $section->name) }}</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
