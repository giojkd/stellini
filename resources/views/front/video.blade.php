@extends('front.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <video width="100%" height="640" controls>
                            <source src="{{ url($video->video) }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
            </div>
            <div class="col-md-6">
                <h1 class="mt-4">{{ $video->name }}</h1>
                <div class="photo-description my-4">
                    {!! $video->description !!}
                </div>
                @include('front.components.leadgenerationform')
            </div>
        </div>
    </div>
@endsection
