@extends('front.main')
@section('content')
<div class="container-fluid">
    @foreach ($pieces->chunk(4) as $chunks)
        <div class="row">
            @foreach ($chunks as $piece)
                <div class="col-6 col-md-3">
                    <a href="{{ $piece->makeUrl($locale) }}" class="product-thumbnail d-block" >
                        <div class="product-thumbnail-head">
                            {{ $piece->name }}
                        </div>
                        <div class="product-thumbnail-body">
                            <img class="img-fluid" src="{{ $piece->cover() }}" alt="">
                        </div>
                        <div class="product-thumbnail-footer"></div>
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach
</div>
@endsection
