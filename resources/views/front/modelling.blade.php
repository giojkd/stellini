@extends('front.main')
@section('content')




 <div class="container-fluid">
        <div class="row">
                <div class="col border-right vh-100 text-center d-flex justify-content-center align-items-center">
                    <div class="d-flex">
                        {{-- <a href="{{ Route('section',['locale' => $locale,'name'=>Str::slug($section->name),'id'=>$section->id]) }}">{{ $section->name }}</a>  --}}
                        <a href="{{ Route('photos',['locale' => $locale]) }}">Photos</a>
                    </div>
                </div>

                <div class="col border-right vh-100 text-center d-flex justify-content-center align-items-center">
                    <div class="d-flex">
                        {{-- <a href="{{ Route('section',['locale' => $locale,'name'=>Str::slug($section->name),'id'=>$section->id]) }}">{{ $section->name }}</a>  --}}
                        <a href="{{ Route('videos',['locale' => $locale]) }}">Videos</a>
                    </div>
                </div>
        </div>
    </div>
@endsection
