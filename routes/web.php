<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


#Route::get('/{locale}/section/{name}/{id}','FrontController@section')->name('section');

Auth::routes();

Route::get('/{locale}/pet-collection','FrontController@products')->name('products');
Route::get('/{locale}/pet-collection/{name}/{id}','FrontController@product')->name('product');

Route::get('/{locale}/art', 'FrontController@artPiecesCategories')->name('artPiecesCategories');
Route::get('/{locale}/art-pieces-category/{name}/{id}', 'FrontController@artPieces')->name('artpiececategory');
Route::get('/{locale}/art-pieces/{name}/{id}', 'FrontController@artPiece')->name('artpiece');

Route::get('/{locale}/modelling','FrontController@modelling')->name('modelling');

Route::get('/{locale}/modelling/photos','FrontController@photos')->name('photos');
Route::get('/{locale}/modelling/private-photos','FrontController@privatePhotos')->name('privatePhotos');
Route::get('/{locale}/modelling/photos/{name}/{id}','FrontController@photo')->name('photo');

Route::get('/{locale}/modelling/videos', 'FrontController@videos')->name('videos');
Route::get('/{locale}/modelling/videos/{name}/{id}', 'FrontController@video')->name('video');



Route::post('send-request','FrontController@sendRequest')->name('sendrequest');


Route::get('/{locale}/thank-you', 'FrontController@Thankyou')->name('thankyou');



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/{locale?}', 'FrontController@home')->name('home');
