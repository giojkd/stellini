<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('section', 'SectionCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('photo', 'PhotoCrudController');
    Route::crud('video', 'VideoCrudController');
    Route::crud('artpiece', 'ArtpieceCrudController');
    Route::crud('artpiececategory', 'ArtpiececategoryCrudController');

    Route::post('media-dropzone', ['uses' => '\App\Http\Controllers\DropzoneUpload@handleDropzoneUpload'])->name('dropzoneUpload');

    Route::crud('client', 'ClientCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('request', 'RequestCrudController');
}); // this should be the absolute last line of this file